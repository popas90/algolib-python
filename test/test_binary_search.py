from algolib.binary_search import binary_search
from algolib.binary_search import binary_insert

def test_binary_search():
    arr = [ 2, 3, 4, 10, 40 ] 
    x = 10
    assert binary_search(arr, x) == 3
    x = 40
    assert binary_search(arr, x) == 4
    x = 2
    assert binary_search(arr, x) == 0
    x = 1
    assert binary_search(arr, x) == -1
    x = 5
    assert binary_search(arr, x) == -1
    x = 100
    assert binary_search(arr, x) == -1

def test_binary_insert():
    arr = [ 2, 3, 4, 10, 40 ] 
    x = 10
    assert binary_insert(arr, x) == 3
    x = 40
    assert binary_insert(arr, x) == 4
    x = 2
    assert binary_insert(arr, x) == 0
    x = 1
    assert binary_insert(arr, x) == 0
    x = 5
    assert binary_insert(arr, x) == 3
    x = 100
    assert binary_insert(arr, x) == 5