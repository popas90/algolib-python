from algolib.mergesort import mergesort, MergeSort
import random

def test_mergesort():
    seq = [12, 11, 13, 5, 6, 7]
    mergesort(seq)
    assert seq == sorted(seq)
    seq = []
    mergesort(seq)
    assert seq == sorted(seq)
    seq = [1]
    mergesort(seq)
    assert seq == sorted(seq)
    seq = [1, 3, 5, 7]
    mergesort(seq)
    assert seq == sorted(seq)
    seq = [6, 5, 3, 1]
    mergesort(seq)
    assert seq == sorted(seq)

def test_MergeSort():
    seq = [12, 11, 13, 5, 6, 7]
    MergeSort(seq).run()
    assert seq == sorted(seq)
    seq = []
    MergeSort(seq).run()
    assert seq == sorted(seq)
    seq = [1]
    MergeSort(seq).run()
    assert seq == sorted(seq)
    seq = [1, 3, 5, 7]
    MergeSort(seq).run()
    assert seq == sorted(seq)
    seq = [6, 5, 3, 1]
    MergeSort(seq).run()
    assert seq == sorted(seq)
    seq = random.sample(range(1, 100), 50)
    MergeSort(seq).run()
    assert seq == sorted(seq)
    seq = random.sample(range(1, 1000), 100)
    MergeSort(seq).run()
    assert seq == sorted(seq)