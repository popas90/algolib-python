from algolib.hashtable import ChainHashMap

def test_ChainHashMap():
    table = ChainHashMap()
    table[2] = "bla"
    assert table[2] == "bla"
    assert len(table) == 1
    table[2] = "blabla"
    assert table[2] == "blabla"
    assert len(table) == 1
    table[10] = "blabla"
    assert table[10] == "blabla"
    assert len(table) == 2