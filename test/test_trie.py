from algolib.trie import Trie


def test_Trie():
    trie = Trie()
    trie.add('bus')
    trie.add('build')
    trie.add('car')
    trie.add('sim')
    assert trie.contains('car')
    assert not trie.contains('cor')
    assert trie.contains('bus')
    assert not trie.contains('bu')
    trie.add('bu')
    assert trie.contains('bu')
    assert not trie.contains('j')
    assert not trie.contains('Bu')