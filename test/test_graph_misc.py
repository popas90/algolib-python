from algolib.graph.graph import UndirectedGraph, DirectedGraph, DirectedWeightedGraph
from algolib.graph.graph_misc import TransitiveClosure, TopologicalSort, TravellingSalesman


def test_TransitiveClosure():
    graph = UndirectedGraph()
    graph.add_edge((0,1))
    graph.add_edge((1,2))
    graph.add_edge((7,2))
    graph.add_edge((4,2))
    graph.add_edge((4,5))
    graph.add_edge((1,6))
    graph.add_edge((3,5))
    graph.add_edge((7,8))
    graph.add_edge((2,8))
    graph.add_edge((8,9))
    tc = TransitiveClosure(graph)
    reach = tc.run()
    print(graph.adj_matrix)
    print(reach)
    assert reach[0][1]
    assert reach[0][2]
    assert reach[0][3]


def test_TopologicalSort():
    graph = DirectedGraph()
    graph.add_edge((1,2))
    graph.add_edge((1,0))
    graph.add_edge((2,8))
    graph.add_edge((0,4))
    graph.add_edge((9,4))
    graph.add_edge((4,6))
    graph.add_edge((4,5))
    graph.add_edge((5,3))
    graph.add_edge((3,8))
    graph.add_edge((6,8))
    ts = TopologicalSort(graph)
    assert ts.run() == [1, 2, 9, 0, 4, 5, 6, 3, 8]


def test_TravellingSalesman():
    graph = DirectedWeightedGraph()
    graph.add_edge((0,1,1))
    graph.add_edge((1,0,2))
    graph.add_edge((0,2,15))
    graph.add_edge((2,0,9))
    graph.add_edge((1,2,7))
    graph.add_edge((2,1,6))
    graph.add_edge((0,3,6))
    graph.add_edge((3,0,10))
    graph.add_edge((1,3,3))
    graph.add_edge((3,1,4))
    graph.add_edge((2,3,12))
    graph.add_edge((3,2,8))
    tsp = TravellingSalesman(graph)
    assert tsp.run() == 21
