from algolib.graph.shortest_path import Floyd, Dijkstra
from algolib.graph.graph import UndirectedWeightedGraph, DirectedWeightedGraph

def test_Floyd():
    graph = UndirectedWeightedGraph()
    graph.add_edge((0,1,2))
    graph.add_edge((1,4,4))
    graph.add_edge((4,5,7))
    graph.add_edge((5,3,3))
    graph.add_edge((3,2,8))
    graph.add_edge((2,0,5))
    graph.add_edge((0,4,6))
    graph.add_edge((3,4,1))
    graph.add_edge((1,3,7))
    floyd = Floyd(graph)
    dist = floyd.run()
    assert dist[0][2] == 5
    assert dist[2][1] == 7
    assert dist[2][5] == 11
    assert dist[0][3] == 7
    assert dist[1][3] == 5

def test_Dijkstra():
    graph = DirectedWeightedGraph()
    graph.add_edge((0,1,8))
    graph.add_edge((0,2,2))
    graph.add_edge((2,1,3))
    graph.add_edge((1,4,1))
    graph.add_edge((1,3,6))
    graph.add_edge((1,5,5))
    graph.add_edge((2,4,3))
    graph.add_edge((3,5,2))
    graph.add_edge((4,5,2))
    dijkstra = Dijkstra(graph)
    dist, _ = dijkstra.run(0)
    print(dist)
    assert dist == [0, 5, 2, 11, 5, 7]