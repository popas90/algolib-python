from algolib.union_find import UnionFind

def test_UnionFind():
    uf = UnionFind(10)
    uf.union(0,1)
    uf.union(1,2)
    assert uf.find(0,2)
    assert not uf.find(2,3)
    uf.union(3,4)
    uf.union(4,0)
    assert uf.find(2,3)