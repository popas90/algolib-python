from algolib.binary_tree import BinaryTreeNode, inorder, preorder, postorder


def test_BinaryTreeNode():
    node = BinaryTreeNode(1)
    node.left = BinaryTreeNode(2)
    node.right = BinaryTreeNode(3)
    node.right.right = BinaryTreeNode(7)
    assert node.isLeaf() == False
    assert node.isParent() == True
    assert node.left.isLeaf() == True
    assert node.right.isParent() == True

def test_preorder(capsys):     
    visit = lambda x : print(x.data)
    node = BinaryTreeNode(1)
    node.left = BinaryTreeNode(2)
    node.right = BinaryTreeNode(3)
    node.left.left = BinaryTreeNode(4)
    node.left.right = BinaryTreeNode(5)
    node.right.right = BinaryTreeNode(7)
    preorder(node, visit)
    captured = capsys.readouterr()   
    assert captured.out == "1\n2\n4\n5\n3\n7\n"

def test_postorder(capsys):     
    visit = lambda x : print(x.data)
    node = BinaryTreeNode(1)
    node.left = BinaryTreeNode(2)
    node.right = BinaryTreeNode(3)
    node.left.left = BinaryTreeNode(4)
    node.left.right = BinaryTreeNode(5)
    node.right.right = BinaryTreeNode(7)
    postorder(node, visit)
    captured = capsys.readouterr()   
    assert captured.out == "4\n5\n2\n7\n3\n1\n"

def test_inorder(capsys):     
    visit = lambda x : print(x.data)
    node = BinaryTreeNode(1)
    node.left = BinaryTreeNode(2)
    node.right = BinaryTreeNode(3)
    node.left.left = BinaryTreeNode(4)
    node.left.right = BinaryTreeNode(5)
    node.right.right = BinaryTreeNode(7)
    inorder(node, visit)
    captured = capsys.readouterr()   
    assert captured.out == "4\n2\n5\n1\n3\n7\n"