from algolib.graph.min_span_tree import Kruskal, Prim
from algolib.graph.graph import UndirectedWeightedGraph

def test_Kruskal():
    graph = UndirectedWeightedGraph()
    graph.add_edge((0,1,2))
    graph.add_edge((1,4,4))
    graph.add_edge((4,5,7))
    graph.add_edge((5,3,3))
    graph.add_edge((3,2,8))
    graph.add_edge((2,0,5))
    graph.add_edge((0,4,6))
    graph.add_edge((3,4,1))
    graph.add_edge((1,3,7))
    kruskal = Kruskal(graph)
    assert kruskal.run() == [(4,3,1), (0,1,2), (5,3,3), (1,4,4), (0,2,5)]

def test_Prim():
    graph = UndirectedWeightedGraph()
    graph.add_edge((0,1,2))
    graph.add_edge((1,4,4))
    graph.add_edge((4,5,7))
    graph.add_edge((5,3,3))
    graph.add_edge((3,2,8))
    graph.add_edge((2,0,5))
    graph.add_edge((0,4,6))
    graph.add_edge((3,4,1))
    graph.add_edge((1,3,7))
    prim = Prim(graph)
    assert sorted(prim.run()) == sorted([(4,3,1), (0,1,2), (3,5,3), (1,4,4), (0,2,5)])