from algolib.quicksort import quicksort, QuickSort
import random

def test_quicksort():
    seq = [12, 11, 13, 5, 6, 7]
    quicksort(seq)
    assert seq == sorted(seq)
    seq = []
    quicksort(seq)
    assert seq == sorted(seq)
    seq = [1]
    quicksort(seq)
    assert seq == sorted(seq)
    seq = [1, 3, 5, 7]
    quicksort(seq)
    assert seq == sorted(seq)
    seq = [6, 5, 3, 1]
    quicksort(seq)
    assert seq == sorted(seq)
    seq = [10, 7, 8, 9, 1, 5] 
    quicksort(seq)
    assert seq == sorted(seq)

def test_QuickSort():
    seq = [12, 11, 13, 5, 6, 7]
    QuickSort(seq).run()
    assert seq == sorted(seq)
    seq = []
    QuickSort(seq).run()
    assert seq == sorted(seq)
    seq = [1]
    QuickSort(seq).run()
    assert seq == sorted(seq)
    seq = [1, 3, 5, 7]
    QuickSort(seq).run()
    assert seq == sorted(seq)
    seq = [6, 5, 3, 1]
    QuickSort(seq).run()
    assert seq == sorted(seq)
    seq = [10, 7, 8, 9, 1, 5] 
    QuickSort(seq).run()
    assert seq == sorted(seq)
    seq = random.sample(range(1, 100), 50)
    QuickSort(seq).run()
    assert seq == sorted(seq)
    seq = random.sample(range(1, 1000), 100)
    QuickSort(seq).run()
    assert seq == sorted(seq)