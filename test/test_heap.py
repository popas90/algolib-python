from algolib.heap import MaxHeap, MinHeap

def test_MaxHeap():
    heap = MaxHeap([4,9,3,10,5,6,2,1,10])
    assert heap.peek() == 10
    assert heap.pop() == 10
    assert heap.pop() == 10
    assert heap.pop() == 9
    assert heap.peek() == 6
    heap.push(11)
    assert heap.peek() == 11
    assert heap.pop () == 11
    assert heap.pop () == 6
    assert heap.pop () == 5
    assert heap.pop () == 4
    assert heap.pop () == 3
    assert heap.pop () == 2
    assert heap.size() == 1
    assert heap.pop () == 1
    assert heap.size() == 0

def test_MinHeap():
    heap = MinHeap([4,9,3,10,5,6,2,1,10])
    assert heap.peek() == 1
    assert heap.pop() == 1
    assert heap.pop() == 2
    assert heap.pop() == 3
    assert heap.peek() == 4
    heap.push(1)
    assert heap.peek() == 1
    assert heap.pop () == 1
    assert heap.pop () == 4
    assert heap.pop () == 5
    assert heap.pop () == 6
