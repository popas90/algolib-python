from algolib.graph.graph import UndirectedGraph
from algolib.graph.search import BreadthFirstSearch, DepthFirstSearch, ConnectedComponent

def test_DFS(capsys):
    graph = UndirectedGraph()
    graph.add_edge((1,2))
    graph.add_edge((7,2))
    graph.add_edge((4,2))
    graph.add_edge((4,5))
    graph.add_edge((1,3))
    graph.add_edge((3,5))
    graph.add_edge((7,8))
    graph.add_edge((2,8))
    graph.add_edge((8,9))
    dfs = DepthFirstSearch(graph, lambda x : print(x))
    dfs.run(1)
    captured = capsys.readouterr()   
    assert captured.out == "1\n3\n5\n4\n2\n7\n8\n9\n"

def test_BFS(capsys):
    graph = UndirectedGraph()
    graph.add_edge((1,2))
    graph.add_edge((7,2))
    graph.add_edge((4,2))
    graph.add_edge((4,5))
    graph.add_edge((1,3))
    graph.add_edge((3,5))
    graph.add_edge((7,8))
    graph.add_edge((2,8))
    graph.add_edge((8,9))
    bfs = BreadthFirstSearch(graph, lambda x : print(x))
    bfs.run(1)
    captured = capsys.readouterr()   
    assert captured.out == "1\n2\n3\n8\n4\n7\n5\n9\n"

def test_ConnectedComponent():
    graph = UndirectedGraph()
    graph.add_edge((1,2))
    graph.add_edge((7,2))
    graph.add_edge((4,2))
    graph.add_edge((4,5))
    graph.add_edge((1,3))
    graph.add_edge((3,5))
    graph.add_edge((7,8))
    graph.add_edge((2,8))
    graph.add_edge((8,9))
    graph.add_edge((8,6))
    cc = ConnectedComponent(graph)
    components = cc.run()
    assert len(components) == 1
    assert components[0] == set([1,2,3,4,5,6,7,8,9])
    graph.add_edge((10,11))
    cc = ConnectedComponent(graph)
    components = cc.run()
    assert len(components) == 2
    assert components[0] == set([1,2,3,4,5,6,7,8,9])
    assert components[1] == set([10,11])
