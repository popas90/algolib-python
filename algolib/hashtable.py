import collections.abc
from random import randrange


class MapBase(collections.abc.MutableMapping):
    class _Item:
        __slots__ = '_key', '_value'

        def __init__(self, k, v):
            self._key = k
            self._value = v
        
        def __eq__(self, other):
            return self._key == other._key

        def __ne__(self, other):
            return not (self == other)

        def __lt__(self, other):
            return self._key < other._key

class HashMapBase(MapBase):

    def __init__(self, cap=11, p=109345121):
        self._table = [UnsortedTableMap()] * cap
        self._count = 0
        self._prime = p
        self._scale = 1 + randrange(p-1)
        self._shift = randrange(p)

    def _hash_function(self, k):
        return (hash(k)*self._scale + self._shift) % self._prime % len(self._table)
    
    def _resize(self, c):
        old = list(self.items())
        self._table = [None] * c
        self._n = 0
        for (k,v) in old:
            self[k] = v

    def __getitem__(self, key):
        j = self._hash_function(key)
        return self._bucket_getitem(j, key)

    def __setitem__(self, key, value):
        j = self._hash_function(key)
        self._bucket_setitem(j, key, value)
        if self._count > len(self._table) // 2:
            self._resize(2 * len(self._table) - 1)

    def __delitem__(self, key):
        j = self._hash_function(key)
        self._bucket_delitem(j, key)
        self._count -= 1

    def __len__(self):
        return self._count
    
    def _bucket_getitem(self, j, key):
        pass

    def _bucket_setitem(self, j, key, value):
        pass

    def _bucket_delitem(self, j, key):
        pass


class UnsortedTableMap(MapBase):
    
    def __init__(self):
        self._table = []

    def __getitem__(self, key):
        for item in self._table:
            if key == item._key:
                return item._value
        raise KeyError('Key Error: {}'.format(key))
            

    def __setitem__(self, key, value):
        for item in self._table:
            if key == item._key:
                item._value = value
                return
        self._table.append(self._Item(key, value))

    def __delitem__(self, key):
        for j in range(len(self._table)):
            if key == self._table[j]._key:
                self._table.pop(j)
                return
        raise KeyError('Key Error: {}'.format(key))

    def __len__(self):
        return len(self._table)

    def __iter__(self):
        for item in self._table:
            yield item._key


class ChainHashMap(HashMapBase):

    def _bucket_getitem(self, j, key):
        bucket = self._table[j]
        if bucket is None:
            raise KeyError('Key Error: {}'.format(key))
        return bucket[key]

    def _bucket_setitem(self, j, key, value):
        if self._table[j] is None:
            self._table[j] = UnsortedTableMap()
        oldsize = len(self._table[j])
        self._table[j][key] = value
        if len(self._table[j]) > oldsize:
            self._count += 1

    def _bucket_delitem(self, j, key):
        bucket = self._table[j]
        if bucket is None:
            raise KeyError('Key Error: {}'.format(key))
        del bucket[key]

    def __iter__(self):
        for bucket in self._table:
            if bucket is not None:
                for key in bucket:
                    yield key
        
