from collections import defaultdict

class TrieNode:

    def __init__(self, end=False):
        self.end = end
        self.children = defaultdict(TrieNode)

    def __str__(self):
        return 'TrieNode, children: {}, end: {}'.format(self.children.keys(), self.end)


class Trie:

    def __init__(self):
        self.root = TrieNode()

    def contains(self, s):
        return self._contains(self.root, s, 0)

    def _contains(self, n, s, i):
        if i == len(s):
            return n.end
        c = s[i]
        return self._contains(n.children[c], s, i+1)

    def add(self, s):
        self.root = self._add(self.root, s, 0)

    def _add(self, n, s, i):
        if i == len(s):
            n.end = True
        else:
            c = s[i]            
            n.children[c] = self._add(n.children[c], s, i+1)
        return n