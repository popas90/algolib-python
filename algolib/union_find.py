class UnionFind:

    def __init__(self, n):
        self._sets = [i for i in range(n)]
        self._sizes = [1 for _ in range(n)]

    def _root(self, i):
        while i != self._sets[i]:
            # path compression
            self._sets[i] = self._sets[self._sets[i]]
            i = self._sets[i]
        return i

    def find(self, p, q):
        return self._root(p) == self._root(q)

    def union(self, p, q):
        rp = self._root(p)
        rq = self._root(q)
        # weighted trees - small tree below large one
        if self._sizes[rp] < self._sizes[rq]:
            self._sets[rp] = rq
            self._sizes[rq] += self._sizes[rp]
        else:
            self._sets[rq] = rp
            self._sizes[rp] += self._sizes[rq]