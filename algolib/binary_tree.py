class BinaryTreeNode:
    def __init__(self, data, lchild=None, rchild=None):
        self.data = data
        self.left = lchild
        self.right = rchild

    def isLeaf(self):
        return self.left == None and self.right == None

    def isParent(self):
        return not self.isLeaf()


def preorder(node, visit):
    if not node:
        return
    visit(node)
    preorder(node.left, visit)    
    preorder(node.right, visit)


def postorder(node, visit):
    if not node:
        return
    postorder(node.left, visit)
    postorder(node.right, visit)
    visit(node)


def inorder(node, visit):
    if not node:
        return
    inorder(node.left, visit)
    visit(node)
    inorder(node.right, visit)
    


