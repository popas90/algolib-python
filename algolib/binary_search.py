def binary_search(seq, elem):
    return _search(seq, elem, 0, len(seq) - 1)

def _search(seq, elem, li, ri):
    if li > ri:
        # Element is not present
        return -1
    
    mid = (li + ri) // 2
    if elem == seq[mid]:
        return mid
    elif elem > seq[mid]:
        return _search(seq, elem, mid + 1, ri)
    else:
        return _search(seq, elem, li, mid-1)

def binary_insert(seq, elem):
    return _insert(seq, elem, 0, len(seq) - 1)

def _insert(seq, elem, li, ri):
    if li > ri:
        return li
    
    mid = (li + ri) // 2
    if elem == seq[mid]:
        return mid
    elif elem > seq[mid]:
        return _insert(seq, elem, mid + 1, ri)
    else:
        return _insert(seq, elem, li, mid-1)
