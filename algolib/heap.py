from abc import ABC, abstractmethod

class HeapBase(ABC):
    def __init__(self, seq=[]):
        self._heap = []
        for s in seq:
            self.push(s)

    @abstractmethod
    def _comp(self, li, ri):
        return 

    def _exch(self, i, j):
        self._heap[i], self._heap[j] = self._heap[j], self._heap[i]

    def _swim(self, k):
        while self._comp(k//2, k):
            self._exch(k//2, k)
            k = k//2

    def _sink(self, k):
        while 2*k < len(self._heap):
            j = 2*k
            if j < len(self._heap)-1 and self._comp(j, j+1):
                j += 1
            if not self._comp(k, j):
                break
            self._exch(k,j)
            k = j
    
    def peek(self):
        if self.size() > 0:
            print(self._heap)
            return self._heap[0]
        else:
            raise Exception('Empty heap')

    def pop(self):
        if self.size() > 0:
            top = self._heap[0]
            self._exch(0, -1)
            self._heap.pop()
            self._sink(0)
            return top
        else:
            raise Exception('Empty heap')

    def push(self, value):
        self._heap.append(value)
        self._swim(len(self._heap) - 1)

    def size(self):
        return len(self._heap)

    def is_empty(self):
        return not (self.size() > 0)


class MaxHeap(HeapBase):

    def __init__(self, lst=[]):
        super().__init__(lst)

    def _comp(self, li, ri):
        return self._heap[li] < self._heap[ri]


class MinHeap(HeapBase):

    def __init__(self, lst=[]):
        super().__init__(lst)

    def _comp(self, li, ri):
        return self._heap[li] > self._heap[ri]


class CustomHeap(HeapBase):

    def __init__(self, lst, compare):
        self._compare = compare
        super().__init__(lst)

    def _comp(self, li, ri):
        return self._compare(self._heap[li], self._heap[ri])


            
