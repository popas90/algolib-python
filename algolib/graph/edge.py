from abc import ABC, abstractmethod

class BaseEdge(ABC):

    def __init__(self, v, u):
        self._from = v
        self._to = u

    def vertices(self):
        return (self._from, self._to)


class Edge(BaseEdge):

    def __init__(self, v, u, w):
        super().__init__(v, u)    

    def either_vert(self):
        return self._from

    def other_vert(self, v):
        if self._from == v:
            return self._to
        return self._from

    def _same_vertices(self, other):
        return ((self._from == other._from) and (self._to == other._to)) or \
            ((self._from == other._to) and (self._to == other._from))

    def __eq__(self, other):
        return self._same_vertices(other)

    def __ne__(self, other):
        return not self == other

    def __hash__(self):
        return hash((self._from, self._to))


class WeightedEdge(Edge):

    def __init__(self, v, u, w):
        super().__init__(v, u)
        self._weight = w

    @property
    def weight(self):
        return self._weight

    def __eq__(self, other):
        return self._same_vertices(other) and self._weight == other._weight

    def __ne__(self, other):
        return not self == other

    def __hash__(self):
        return hash((self._from, self._to, self._weight))

    def __lt__(self, other):
        return self._weight < other._weight


class DirectedEdge(BaseEdge):

    def __init__(self, v, u):
        super().__init__(v, u)

    @property
    def from_vert(self):
        return self._from

    @property
    def to_vert(self):
        return self._to

    def _same_vertices_and_dir(self, other):
        return (self._from == other._from) and (self._to == other._to)

    def __eq__(self, other):
        return self._same_vertices_and_dir(other)

    def __ne__(self, other):
        return not self == other

    def __hash__(self):
        return hash((self._from, self._to))


class DirectedWeightedEdge(DirectedEdge):

    def __init__(self, v, u, w):
        super().__init__(v, u)
        self._weight = w

    @property
    def weight(self):
        return self._weight

    def __eq__(self, other):
        return self._same_vertices_and_dir(other) and self._weight == other._weight

    def __ne__(self, other):
        return not self == other

    def __hash__(self):
        return hash((self._from, self._to, self._weight))

    def __lt__(self, other):
        return self._weight < other._weight