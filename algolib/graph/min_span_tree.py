from algolib.union_find import UnionFind
from algolib.heap import CustomHeap

class Kruskal:

    def __init__(self, graph):
        self.graph = graph

    def run(self):
        msp = []
        uf = UnionFind(len(self.graph.vertices))
        edges = self.graph.edges
        edges.sort(key=lambda e : e[2])
        for (v,u,w) in edges:
            if not uf.find(v,u):
                msp.append((v,u,w))
                uf.union(v,u)
        return msp


class Prim:
    
    def __init__(self, graph):
        self.graph = graph

    def run(self):
        vertices = self.graph.vertices
        first_vertex = vertices.pop(0)
        pq = CustomHeap(self.graph.edges_from(first_vertex), lambda e0, e1: e0[2] > e1[2])
        msp_vertices = set([first_vertex])
        msp_edges = []
        while vertices:
            candidate = pq.pop()
            if candidate[1] not in msp_vertices:
                msp_vertices.add(candidate[1])
                vertices.remove(candidate[1])
                msp_edges.append(candidate)
                for e in self.graph.edges_from(candidate[1]):
                    pq.push(e) 
            else:
                continue
        return msp_edges
        