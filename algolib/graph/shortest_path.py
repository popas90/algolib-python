import sys
from algolib.heap import MinHeap

class Floyd:

    def __init__(self, graph):
        self._graph = graph

    def run(self):
        adj_matrix = self._graph.adj_matrix
        total_vertices = len(adj_matrix)
        for k in range(total_vertices):
            for i in range(total_vertices):
                for j in range(total_vertices):
                    through_k = adj_matrix[i][k] + adj_matrix[k][j]
                    if (through_k < adj_matrix[i][j]):
                        adj_matrix[i][j] = through_k
        return adj_matrix


class Dijkstra:

    def __init__(self, graph):
        self._graph = graph

    def run(self, source):
        pred = [None] * len(self._graph.vertices)
        dist = [sys.maxsize] * len(self._graph.vertices)
        pq = MinHeap()
        dist[source] = 0
        pq.push((dist[source], source))
        visited = set()

        while not pq.is_empty():
            _, v = pq.pop()
            if v in visited:
                continue
            visited.add(v)
            for edge in self._graph.edges_from(v):
                u = edge.to_vert
                if dist[u] > dist[v] + edge.weight:
                    dist[u] = dist[v] + edge.weight
                    pred[u] = edge
                    pq.push((dist[u], u))
        return dist, pred


class BellmanFord:

    def __init__(self, graph):
        self._graph = graph

    def run(self):
        pass
