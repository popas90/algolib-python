import sys
from abc import ABC, abstractmethod
from collections import defaultdict
from collections import deque
from algolib.graph.edge import Edge, WeightedEdge, DirectedEdge, DirectedWeightedEdge

class Graph(ABC):

    def __init__(self, edges=[], vertex_cnt=0):
        self._adj = defaultdict(set)
        self._populate(edges, vertex_cnt)

    def _populate(self, edges, vertex_cnt):
        if vertex_cnt > 0:
            for i in range(vertex_cnt):
                self._adj[i] = []
        for edge in edges:          
            self.add_edge(edge)
    
    @property
    def vertices(self):
        return list(self._adj.keys())

    @property
    def edges(self):
        edges = set()
        for edges in self._adj.values():
            for e in edges:
                if e in edges:
                    continue
                edges.add(e)
        return list(edges) 

    @abstractmethod
    def add_edge(self, edge):
        pass

    def add_vertex(self, v):
        if v not in self._adj:
            self._adj[v] = set()

    #@abstractmethod
    #def remove_edge(self, e):
    #    pass

    def remove_vertex(self, v):
        self._adj.pop(v)
        for edges in self._adj.values():
            edges.remove(v)

    def neighbors(self, v):
        return self._adj[v]

    def edges_from(self, v):
        return self._adj[v]


class UndirectedGraph(Graph):

    def add_edge(self, edge):
        v, u = edge[0], edge[1]
        self.add_vertex(v)
        self.add_vertex(u)
        self._adj[v].add(u)
        self._adj[u].add(v)

    def remove_edge(self, e):
        self._adj[e[0]].remove(e[1])
        self._adj[e[1]].remove(e[0])

    def edges_from(self, v):
        edges = self._adj[v]
        return [(v, u, w) for (u, w) in edges]

    @property
    def adj_matrix(self):
        vcnt = len(self.vertices)
        matrix = [[False]*vcnt for _ in range(vcnt)]
        for v,us in self._adj.items():
            for u in us:
                matrix[v][v] = True
                matrix[u][u] = True
                matrix[v][u] = True
                matrix[u][v] = True
        return matrix


class UndirectedWeightedGraph(UndirectedGraph):

    @property
    def edges(self):
        edges = set()
        for v,us in self._adj.items():
            for uw in us:
                (u,w) = uw 
                if (v,u,w) in edges or (u,v,w) in edges:
                    continue
                edges.add((v,u,w))
        return list(edges) 

    def add_edge(self, edge):
        v, u, w = edge[0], edge[1], edge[2]
        self.add_vertex(v)
        self.add_vertex(u)
        self._adj[v].add((u,w))
        self._adj[u].add((v,w))

    @property
    def adj_matrix(self):
        vcnt = len(self.vertices)
        matrix = [[sys.maxsize]*vcnt for _ in range(vcnt)]
        for v,us in self._adj.items():
            for uw in us:
                (u,w) = uw
                matrix[v][u] = w
                matrix[u][v] = w
        return matrix


class DirectedWeightedGraph(Graph):

    def add_edge(self, edge):
        v, u, w = edge[0], edge[1], edge[2]
        self.add_vertex(v)
        self.add_vertex(u)
        self._adj[v].add(DirectedWeightedEdge(v, u, w))

    @property
    def adj_matrix(self):
        vcnt = len(self.vertices)
        matrix = [[0]*vcnt for _ in range(vcnt)]
        for v,us in self._adj.items():
            for uw in us:
                matrix[v][uw.to_vert] = uw.weight
        return matrix


class DirectedGraph(Graph):

    def add_edge(self, edge):
        v, u = edge[0], edge[1]
        self.add_vertex(v)
        self.add_vertex(u)
        self._adj[v].add(u)

    def remove_edge(self, e):
        v, u = e
        self._adj[v].remove(u)