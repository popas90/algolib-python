from algolib.graph.graph import UndirectedGraph
from collections import deque

class BreadthFirstSearch:

    def __init__(self, graph, visit):
        self._graph = graph
        self._action = visit

    def run(self, start):
        queue = deque()
        queue.appendleft(start)
        visited = set()
        while queue:
            v = queue.pop()
            if v not in visited:
                self._action(v)
                visited.add(v)
                for u in self._graph.neighbors(v):
                    if u not in visited:
                        queue.appendleft(u)


class DepthFirstSearch:

    def __init__(self, graph, visit):
        self._graph = graph
        self._action = visit

    def run(self, start):
        stack = deque()
        stack.append(start)
        visited = set()
        while stack:
            v = stack.pop()
            if v not in visited:
                self._action(v)
                visited.add(v)
                for u in self._graph.neighbors(v):
                    if u not in visited:
                        stack.append(u)

class ConnectedComponent:

    def __init__(self, graph):
        self._graph = graph
        self._components = []

    def _add_to_component(self, v):
        self._visited.add(v)
        self._components[-1].add(v)

    def run(self):
        self._visited = set()
        for v in self._graph._adj:
            if v not in self._visited:
                self._components.append(set())
                bfs = BreadthFirstSearch(self._graph, lambda v : self._add_to_component(v))
                bfs.run(v)
        return self._components