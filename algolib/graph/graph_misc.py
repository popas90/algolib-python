from collections import defaultdict
from itertools import combinations
import sys

class TransitiveClosure:

    def __init__(self, graph):
        self._graph = graph

    def run(self):
        adj = self._graph.adj_matrix
        total_vertices = len(adj)
        reach = adj
        
        for k in range(total_vertices):
            for i in range(total_vertices):
                for j in range(total_vertices):
                    reach[i][j] = reach[i][j] or (reach[i][k] and reach[k][j])
        return reach


class TopologicalSort:
    # Kahn's algorithm

    def __init__(self, graph):
        self._graph = graph

    def run(self):
        incoming = defaultdict(set)
        for v, us in self._graph._adj.items():
            for u in us:
                incoming[u].add(v)
        print(incoming)
        res = []
        st = set()
        for v in self._graph.vertices:
            if len(incoming[v]) == 0:
                st.add(v)
                incoming.pop(v)
        while st:
            v = st.pop()
            res.append(v)
            for u in self._graph.neighbors(v):
                #self._graph.remove_edge((v, u))
                incoming[u].remove(v)
                if len(incoming[u]) == 0:
                    st.add(u)
                    incoming.pop(u)
        if incoming:
            return "ERROR"
        return res


class TravellingSalesman:
    # Held-Karp algorithm
    # Recursive formulation:
    # Every subpath of a path of minimum distance is itself of minimum distance.

    def __init__(self, graph):
        self._graph = graph

    def run(self):
        # Assume that cycle always starts at node 0
        dist = self._graph.adj_matrix
        verts = len(dist)
        path = defaultdict(int)

        # s is the number of nodes in subpath, except start and finish nodes
        # s == 0
        for j in range(1, verts):
            path[(j, frozenset())] = dist[0][j]

        # s == 1
        for j in range(1, verts):
            for i in range(1, verts):
                if i != j:
                    path[(i, frozenset([j]))] = dist[0][j] + dist[j][i]
        
        # s >= 1
        for s in range(2, verts):
            for st in combinations(range(1,verts), s):
                for j in set(range(1, verts)) - set(st):
                    minim = sys.maxsize
                    for i in list(st):
                        sst = list(st)
                        sst.remove(i)
                        #print((i, frozenset(sst)))
                        curr = path[(i, frozenset(sst))] + dist[i][j]
                        if minim > curr:
                            minim = curr
                        #print(minim)
                    path[(j, frozenset(st))] = minim

        # close the cycle back to node 0
        minim = sys.maxsize
        st = frozenset(range(1, verts))
        for i in list(st):
            sst = list(st)
            sst.remove(i)
            curr = path[(i, frozenset(sst))] + dist[i][0]
            if minim > curr:
                minim = curr
        path[(0, st)] = minim

        return path[(0, st)]



        


