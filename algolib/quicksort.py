import random

def quicksort(seq):
    _quick(seq, 0, len(seq)-1)

def _quick(seq, lo, hi):
    if lo >= hi:
        return
    pi = _partition(seq, lo, hi)
    _quick(seq, lo, pi-1)
    _quick(seq, pi+1, hi)

def _partition(seq, lo, hi):
    pivot = seq[hi]
    i = lo-1

    for j in range(lo, hi):
        # If current element is smaller than or equal to pivot
        if seq[j] <= pivot:
            # increment index of smaller element
            i += 1
            seq[i], seq[j] = seq[j], seq[i]
    
    seq[i+1], seq[hi] = seq[hi], seq[i+1] 
    return i+1
    
# Translated from R.Sedgewick - Algorithms, 4th ed., pag. 289
class QuickSort: 

    def __init__(self, seq):
        self._seq = seq
        random.shuffle(self._seq)

    def run(self):        
        self._sort(self._seq, 0, len(self._seq)-1)

    def _sort(self, seq, lo, hi):
        # Sort seq[lo..hi]
        if hi <= lo:
            return
        j = self._partition(seq, lo, hi)
        self._sort(seq, lo, j-1)
        self._sort(seq, j+1, hi)

    def _partition(self, seq, lo, hi):
        # Partition into a[lo..i-1], a[i], a[i+1..hi]
        v = seq[lo] # partitioning item
        i, j = lo+1, hi        
        while True:
            # Scan right, scan left, check for scan complete, and exchange
            while i < hi and seq[i] < v:
                i += 1
            while j > lo and v < seq[j]:
                j -= 1
            if i >= j:
                break
            seq[i], seq[j] = seq[j], seq[i]
        seq[lo], seq[j] = seq[j], seq[lo]
        return j
            
