def mergesort(seq):
    if len(seq) <= 1:
        return
    
    mid = len(seq) // 2
    lside = seq[:mid]
    rside = seq[mid:]
    mergesort(lside)
    mergesort(rside)
    merge(seq, lside, rside)

def merge(seq, lside, rside):
    i = j = k = 0
    while i < len(lside) and j < len(rside): 
            if lside[i] < rside[j]: 
                seq[k] = lside[i] 
                i+=1
            else: 
                seq[k] = rside[j] 
                j+=1
            k+=1
          
    # Checking if any element was left 
    while i < len(lside): 
        seq[k] = lside[i] 
        i+=1
        k+=1
          
    while j < len(rside): 
        seq[k] = rside[j] 
        j+=1
        k+=1

# Translated from R.Sedgewick - Algorithms, 4th ed., pag. 273
class MergeSort:

    def __init__(self, seq):
        self._seq = seq
        self._aux = []
        for i in seq:
            self._aux.append(i)

    def run(self):
        self._sort(self._seq, 0, len(self._seq)-1)

    def _sort(self, seq, lo, hi):
        # Sort seq[lo..hi]
        if hi <= lo:
            return
        mid = (lo + hi)//2
        self._sort(seq, lo, mid)
        self._sort(seq, mid+1, hi)
        self._merge(seq, lo, mid, hi)

    def _merge(self, seq, lo, mid, hi):
        # Merge seq[lo..mid] with seq[mid+1..hi]
        i, j = lo, mid+1

        for k in range(lo, hi+1):
            # Copy seq[lo..hi] to aux[lo..hi]
            self._aux[k] = seq[k] 
            
        for k in range(lo, hi+1):
            if i > mid:
                seq[k] = self._aux[j]
                j += 1                
            elif j > hi:
                seq[k] = self._aux[i]
                i += 1                
            elif self._aux[j] < self._aux[i]:
                seq[k] = self._aux[j]
                j += 1                
            else:
                seq[k] = self._aux[i]
                i += 1
        

    